#!/usr/bin/env python

from codecs import encode
from htmlentitydefs import name2codepoint
from xml.dom import minidom
import re, zipfile

def htmlentitydecode(s):
  return re.sub('&(%s);' % '|'.join(name2codepoint),
                lambda m: unichr(name2codepoint[m.group(1)]), s)


class Book:
  def __init__(self, bookTitle, bookAuthor, bookID):
    self._debug = False
    self._title = bookTitle
    self._author = bookAuthor
    self._uniqueid = bookID
    self._pubdate = None
    self._chapters = list(dict())

  def setDebugging(self, flag):
    self._debug = flag

  def addChapter(self, chapterTitle, chapterContent, chapterSubtitle=None, chapterTOCTitle=None):
    chpt = {"title": chapterTitle,
    "subtitle": chapterSubtitle,
    "content": chapterContent,
    "toctitle": chapterTOCTitle,
    }
    self._chapters.append(chpt)

  def getChapterTitles(self):
    titles = list()
    for chpt in self._chapters:
      titles.append(chpt["title"])
    return titles

  def getTOCTitles(self):
    titles = list()
    for chpt in self._chapters:
      t = chpt["toctitle"] if chpt["toctitle"] else chpt["title"]
      titles.append(t)
    return titles

  def generateEPUB(self, outputFile):
    # generate the META-INF/container.xml -
    container = self._generateContainerXML()
    # generate OEBPS/chapter*
    chapterfiles = self._generateChapterXHTML()
    # generate OEBPS/book.opf -
    bookopf = self._generateBookOPFXML(chapterfiles)
    # generate OEBPS/toc.ncx -
    tocncx = self._generateTOCNCXXML(chapterfiles)

    zf = zipfile.ZipFile(outputFile, "w")

    # needs to be STOREed
    zf.writestr("mimetype", "application/epub+zip".encode("ascii"))

    # DEFLATEd
    containerinfo = zipfile.ZipInfo("META-INF/container.xml")
    containerinfo.compress_type = zipfile.ZIP_DEFLATED
    zf.writestr(containerinfo, container)

    bookopfinfo = zipfile.ZipInfo("OEBPS/book.opf")
    bookopfinfo.compress_type = zipfile.ZIP_DEFLATED
    zf.writestr(bookopfinfo, bookopf)

    tocncxinfo = zipfile.ZipInfo("OEBPS/toc.ncx")
    tocncxinfo.compress_type = zipfile.ZIP_DEFLATED
    zf.writestr(tocncxinfo, tocncx)

    for f in chapterfiles:
      finfo = zipfile.ZipInfo("OEBPS/%s" % f["filename"])
      finfo.compress_type = zipfile.ZIP_DEFLATED
      zf.writestr(finfo, f["content"])
    zf.close()

  def _generateContainerXML(self):
    #<?xml version="1.0" encoding="UTF-8" ?>
    #<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">
    #  <rootfiles>
    #    <rootfile full-path="OPS/book.opf" media-type="application/oebps-package+xml"/>
    #  </rootfiles>
    #</container>
    impl = minidom.getDOMImplementation(None)

    container = impl.createDocument(None, "container", None)
    container.childNodes[0].setAttribute("version", "1.0")
    container.childNodes[0].setAttribute("xmlns", "urn:oasis:names:tc:opendocument:xmlns:container")
    rootfiles = container.createElement("rootfiles")
    container.childNodes[0].appendChild(rootfiles)

    rf = container.createElement("rootfile")
    rf.setAttribute("full-path", "OEBPS/book.opf")
    rf.setAttribute("media-type", "application/oebps-package+xml")
    rootfiles.appendChild(rf)

    # get this back as a string so we can nuke minidom's tree
    if self._debug:
      flattened = container.toprettyxml(encoding="UTF-8")
    else:
      flattened = container.toxml(encoding="UTF-8")

    container.unlink()
    return flattened

  def _generateBookOPFXML(self, chapterFiles):
    # the book file that lays out the content of this book

    impl = minidom.getDOMImplementation(None)

    bookopf = impl.createDocument(None, "package", None)
    bookopf.childNodes[0].setAttribute("version", "2.0")
    bookopf.childNodes[0].setAttribute("xmlns", "http://www.idpf.org/2007/opf")
    bookopf.childNodes[0].setAttribute("unique-identifier", "BookId")

    bookopf_metadata = bookopf.createElement("metadata")
    bookopf_metadata.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/")
    bookopf_metadata.setAttribute("xmlns:opf", "http://www.idpf.org/2007/opf")
    dctitle = bookopf.createElement("dc:title")
    dctitle.appendChild(bookopf.createTextNode(self._title))
    bookopf_metadata.appendChild(dctitle)
    dclanguage = bookopf.createElement("dc:language")
    dclanguage.appendChild(bookopf.createTextNode("en"))
    bookopf_metadata.appendChild(dclanguage)
    dcidentifier = bookopf.createElement("dc:identifier")
    dcidentifier.setAttribute("id", "BookId")
    dcidentifier.setAttribute("opf:scheme", "made-up")
    dcidentifier.appendChild(bookopf.createTextNode(self._uniqueid))
    bookopf_metadata.appendChild(dcidentifier)
    dccreator = bookopf.createElement("dc:creator")
    dccreator.setAttribute("opf:role", "aut")
    dccreator.appendChild(bookopf.createTextNode(self._author))
    bookopf_metadata.appendChild(dccreator)

    bookopf_manifest = bookopf.createElement("manifest")

    for chpt in chapterFiles:
      manifestitem = bookopf.createElement("item")
      manifestitem.setAttribute("id", chpt["idname"])
      manifestitem.setAttribute("href", chpt["filename"])
      manifestitem.setAttribute("media-type", "application/xhtml+xml")
      bookopf_manifest.appendChild(manifestitem)

    # TOC
    ncx = bookopf.createElement("item")
    ncx.setAttribute("id", "ncx")
    ncx.setAttribute("href", "toc.ncx")
    ncx.setAttribute("media-type", "application/x-dtbncx+xml")
    bookopf_manifest.appendChild(ncx)

    bookopf_spine = bookopf.createElement("spine")
    bookopf_spine.setAttribute("toc", "ncx")
    for chpt in chapterFiles:
      itemref = bookopf.createElement("itemref")
      itemref.setAttribute("idref", chpt["idname"])
      bookopf_spine.appendChild(itemref)

    #optional
    #bookopf_guide = bookopf.createElement("guide")

    bookopf.childNodes[0].appendChild(bookopf_metadata)
    bookopf.childNodes[0].appendChild(bookopf_manifest)
    bookopf.childNodes[0].appendChild(bookopf_spine)
    #bookopf.childNodes[0].appendChild(bookopf_guide)

    if self._debug:
      flattened = bookopf.toprettyxml(encoding="UTF-8")
    else:
      flattened = bookopf.toxml(encoding="UTF-8")

    bookopf.unlink()
    return flattened

  def _generateTOCNCXXML(self, chapterFiles):
    impl = minidom.getDOMImplementation(None)

    #tocncx = impl.createDocument('http://www.daisy.org/z3986/2005/ncx/', 'ncx', ncx_dt)
    tocncx = impl.createDocument(None, 'ncx', None)
    tocncx.childNodes[0].setAttribute("version", "2005-1")
    tocncx.childNodes[0].setAttribute("xmlns", "http://www.daisy.org/z3986/2005/ncx/")
    tocncx.childNodes[0].setAttribute("xml:lang", "en")

    ncxhead = tocncx.createElement("head")
    ncxmetauid = tocncx.createElement("meta")
    ncxmetauid.setAttribute("name", "dtb:uid")
    #must be same as unique id in opf
    ncxmetauid.setAttribute("content", self._uniqueid)
    ncxhead.appendChild(ncxmetauid)
    ncxmetadepth = tocncx.createElement("meta")
    ncxmetadepth.setAttribute("name", "dtb:depth")
    ncxmetadepth.setAttribute("content", "1")
    ncxhead.appendChild(ncxmetadepth)
    ncxmetapgs = tocncx.createElement("meta")
    ncxmetapgs.setAttribute("name", "dtb:totalPageCount")
    ncxmetapgs.setAttribute("content", "0")
    ncxhead.appendChild(ncxmetapgs)
    ncxmetamaxpgs = tocncx.createElement("meta")
    ncxmetamaxpgs.setAttribute("name", "dtb:maxPageNumber")
    ncxmetamaxpgs.setAttribute("content", "0")
    ncxhead.appendChild(ncxmetamaxpgs)
    tocncx.childNodes[0].appendChild(ncxhead)

    ncxdocTitle = tocncx.createElement("docTitle")
    ncxdocTitleText = tocncx.createElement("text")
    ncxdocTitleText.appendChild(tocncx.createTextNode(self._title))
    ncxdocTitle.appendChild(ncxdocTitleText)
    tocncx.childNodes[0].appendChild(ncxdocTitle)

    ncxdocAuthor = tocncx.createElement("docAuthor")
    ncxdocAuthorText = tocncx.createElement("text")
    ncxdocAuthorText.appendChild(tocncx.createTextNode(self._author))
    ncxdocAuthor.appendChild(ncxdocAuthorText)
    tocncx.childNodes[0].appendChild(ncxdocAuthor)

    # <navMap>
    #   <navPoint class="chapter" id="chapter1" playOrder="1">
    #     <navLabel><text>Chapter 1</text></navLabel>
    #     <content src="chapter1.xhtml"/>
    #   </navPoint>
    # </navMap>
    count = 0
    ncxnavMap = tocncx.createElement("navMap")
    for chpt in chapterFiles:
      mappt = tocncx.createElement("navPoint")
      mappt.setAttribute("class", "chapter")
      mappt.setAttribute("id", chpt["idname"])
      order = "%d" % (count + 1)
      mappt.setAttribute("playOrder", order)
      mapptnavLabel = tocncx.createElement("navLabel")
      mapptnavLabelText = tocncx.createElement("text")
      mapptnavLabelText.appendChild(tocncx.createTextNode(chpt["toctitle"]))
      mapptnavLabel.appendChild(mapptnavLabelText)
      mappt.appendChild(mapptnavLabel)
      mapptcontent = tocncx.createElement("content")
      mapptcontent.setAttribute("src", chpt["filename"])
      mappt.appendChild(mapptcontent)
      ncxnavMap.appendChild(mappt)
      count = count + 1
    tocncx.childNodes[0].appendChild(ncxnavMap)

    if self._debug:
      flattened = tocncx.toprettyxml(encoding="UTF-8")
    else:
      flattened = tocncx.toxml(encoding="UTF-8")

    tocncx.unlink()
    return flattened

  def _generateChapterXHTML(self):
    chapters = list(dict())
    count = 0

    for chpt in self._chapters:
      thischpt = dict()
      thischpt["filename"] = "chapter_%d.xhtml" % count
      thischpt["idname"] = "chapter%d" % count
      thischpt["toctitle"] = chpt["toctitle"] if chpt["toctitle"] else chpt["title"]

      impl = minidom.getDOMImplementation(None)

      xhtml_dt = impl.createDocumentType('html',
                                         '-//W3C//DTD XHTML 1.0 Transitional//EN',
                                         'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd')
      doc = impl.createDocument('http://www.w3.org/1999/xhtml', 'html', xhtml_dt)

      doc.childNodes[1].setAttribute("xmlns", "http://www.w3.org/1999/xhtml")
      doc.childNodes[1].setAttribute("xml:lang", "en")

      # let's assemble us some xhtml!
      xhtmlheader = doc.createElement("head")
      titleelement = doc.createElement("title")
      titletext = doc.createTextNode(chpt["title"])
      titleelement.appendChild(titletext)
      xhtmlheader.appendChild(titleelement)

      # add this mess to the html element
      doc.childNodes[1].appendChild(xhtmlheader)
      body = doc.createElement("body")

      h1 = doc.createElement("h1")
      h1text = doc.createTextNode(chpt["title"])
      h1.appendChild(h1text)
      body.appendChild(h1)

      div = doc.createElement("div")
      div.appendChild(doc.createTextNode(chpt["content"]))
      body.appendChild(div)
      
      doc.childNodes[1].appendChild(body)

      if self._debug:
        thischpt["content"] = htmlentitydecode(doc.toprettyxml(encoding="UTF-8").decode("UTF-8")).encode("UTF-8")
      else:
        thischpt["content"] = htmlentitydecode(doc.toxml(encoding="UTF-8").decode("UTF-8")).encode("UTF-8")
      doc.unlink()

      chapters.append(thischpt)
      count = count + 1
    return chapters

if __name__ == "__main__":
  print "import epubassemble"
