The epubassemble.py does not require anything outside of Python 2.6/2.7 stdlib,
however the demo.py driver program does.

To install the two module requirements (feedparser and BeautifulSoup),
use:

    pip install -r requirements.txt


'epubassemble' aggregates [X]HTML documents into chapters, then produces the
XML headers and ZIP-file container format needed for readers to handle the
documents enclosed. Interesting future work could be to incorporate images in
normal entries, as well as "podcast" feed content properly. Examine the demo
program for calling sequence of the 'epubassemble' module.

To run the demo, you will need the URL for a feed (RSS, Atom, CDF) that
you would like to turn into an 'epub' document. Each document in the feed
will be turned into a chapter in the output epub document, in reverse
chronological order. I have found that this works best when the feed
provides the long-form / full-text of each post, rather than a summary.

Execute the demo like this:

    ./demo.py \
      http://static.fsf.org/fsforg/rss/blogs.xml \
      fsf-blogs-$(date +%Y%m%d).epub


    ./demo.py \
      http://daringfireball.net/feeds/articles \
      daringfireball-articles-$(date +%Y%m%d).epub
