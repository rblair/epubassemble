#!/usr/bin/env python

import feedparser, hashlib, sys
from BeautifulSoup import BeautifulSoup
from epubassemble import Book

feed = feedparser.parse(sys.argv[1])

if(feed["bozo"] == 1):
  print "feed %s trips the bozo bit" % sys.argv[1]
  exit()

booktitle = feed["feed"]["title"]
bookauthor = feed["feed"]["link"]

if feed["feed"].has_key("id"):
  bookuniqueid = feed["feed"]["id"]
elif feed["feed"].has_key("link"):
  bookuniqueid = hashlib.sha1(feed["feed"]["link"]).hexdigest()
else:
  bookuniqueid = hashlib.sha1(feed["feed"]["title"]).hexdigest()

book = Book(booktitle, bookauthor, bookuniqueid)

print "Generating epub for:"
print ("%s: %s") % (feed["feed"]["title"], feed["feed"]["link"])

# reverse cronological
#for entry in feed["entries"]:

# "normal" cronological
for entry in reversed(feed["entries"]):
  title = entry["title"]

  # need to check entry.keys() for content, summary_detail, summary...
  # not all entries have the "content" key
  for itemtype in ("content", "summary_detail", "summary"):
    if entry.has_key(itemtype):
      if(itemtype == "content"):
        content = ""
        for subent in entry["content"]:
          #content = content + subent["value"]
          content = unicode(BeautifulSoup(subent["value"]))
          pass
      elif (itemtype == "summary_detail"):
        #content = content + entry["summary_detail"]["value"]
        content = unicode(BeautifulSoup(entry["summary_detail"]["value"]))
      elif (itemtype == "summary"):
        #content = entry["summary"]
        content = unicode(BeautifulSoup(entry["summary"]))
      else:
        # yay, empty chapter?
        content = ""
        pass

  if entry.has_key("subtitle"):
    subtitle = entry["subtitle"]
  else:
    subtitle = None

  updated = entry["updated_parsed"]
  updated_date = "%d/%02d/%02d %02d:%02d" % (updated.tm_year, updated.tm_mon,
                                     updated.tm_mday, updated.tm_hour,
                                     updated.tm_min)
  toctitle = "%s - %s" % (updated_date, entry["title"])
  book.addChapter(title, content, chapterSubtitle=subtitle, chapterTOCTitle=toctitle)

#print book.getTOCTitles()
book.generateEPUB(sys.argv[2])
